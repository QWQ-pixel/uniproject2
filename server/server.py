import http.server
import socketserver

handler = http.server.SimpleHTTPRequestHandler

with socketserver.TCPServer(("", 1234), handler) as httpd:
   try:
      httpd.serve_forever()
   except Exception:
      httpd.shutdown()