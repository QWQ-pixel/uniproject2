import urllib.request

target = "http://localhost:1234/"
req = urllib.request.Request(url=target)
fp = urllib.request.urlopen(req)

content = fp.read().decode("utf-8")
strings = content.split("\n") 
index = 0
table1 = []

for i in range(len(strings)):
    if strings[i] == "":
        a = i+2
        while a<len(strings):
            table1.append(strings[a])
            a+=1

def count_words(text, word):
    r = 0
    for string in text:
       r += string.count(word)
    return r

count = {}

for string in table1:
    count[string.split()[2]] = count_words(table1, string.split()[2])

index = 1
print("| №п/п | Предмет | Кол-во сдач |")

for lesson, num in count.items():
    print("| %d    | %s | %d |"%(index, lesson, num))
    index += 1

fp.close()